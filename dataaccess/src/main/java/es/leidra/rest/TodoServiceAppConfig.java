package es.leidra.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoServiceAppConfig {
	public static void main(String[] args) {
		SpringApplication.run(TodoServiceAppConfig.class, args);
	}
}
