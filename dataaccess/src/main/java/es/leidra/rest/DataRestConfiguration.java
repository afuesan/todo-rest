package es.leidra.rest;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

import es.leidra.rest.entities.Todo;

@Configuration
@Import(RepositoryRestMvcConfiguration.class)
public class DataRestConfiguration extends RepositoryRestMvcConfiguration {

	public DataRestConfiguration() {
		super();
	}

	@Bean
	@ConfigurationProperties(prefix = "spring.data.rest")
	@Override
	public RepositoryRestConfiguration config() {
		return super.config();
	}

	@Override
	protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		super.configureRepositoryRestConfiguration(config);
		config.setBaseUri("/api");
		config.exposeIdsFor(Todo.class);
	}
}
