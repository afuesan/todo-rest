package es.leidra.rest.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import es.leidra.rest.entities.Todo;

@RepositoryRestResource(path = "todos")
public interface TodoRepository extends PagingAndSortingRepository<Todo, Long> {
	List<Todo> findAllByTitle(@Param("title") String title);
}
