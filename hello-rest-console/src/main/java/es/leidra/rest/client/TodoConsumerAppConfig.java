package es.leidra.rest.client;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.leidra.rest.client.dtos.TodoDto;
import es.leidra.rest.client.dtos.TodoList;

public class TodoConsumerAppConfig {

	public static void main(String[] args) throws URISyntaxException {
		RestTemplate restTemplate = restTemplateFactory();
		URI rootUri = new URI("http://dataaccess:8080/api/todos");

		saveTodo(rootUri, restTemplate);
		retrieveTodo(restTemplate, rootUri);
		exchangeTodos(restTemplate, rootUri);
		findTodos(restTemplate, rootUri);
	}

	private static void exchangeTodos(RestTemplate restTemplate, URI rootUri) {
		String findUri = rootUri + "/search/findAllByTitle?title=tarea";

		ResponseEntity<Resources<TodoDto>> result = restTemplate.exchange(findUri, HttpMethod.GET, null, new ParameterizedTypeReference<Resources<TodoDto>>() {
		});
		Assert.notNull(result, "No result found for tarea");

		result.getBody().forEach(todo -> {
			System.out.println("Description: " + todo.getDescription());
			System.out.println("Title: " + todo.getTitle());
		});
	}

	private static void findTodos(RestTemplate restTemplate, URI rootUri) {
		String findUri = rootUri + "/search/findAllByTitle?title={title}";

		TodoList result = restTemplate.getForObject(findUri, TodoList.class, "tarea");
		Assert.notNull(result, "No result found for tarea");

		result.getContent().forEach(todo -> {
			System.out.println("Description: " + todo.getDescription());
			System.out.println("Title: " + todo.getTitle());
		});
	}

	private static void retrieveTodo(RestTemplate restTemplate, URI rootUri) {
		TodoDto dto = restTemplate.getForObject(rootUri + "/{id}", TodoDto.class, 1l);
		Assert.notNull(dto, "No result found for 1");
		System.out.println("Description: " + dto.getDescription());
		System.out.println("Title: " + dto.getTitle());
	}

	private static void saveTodo(URI rootUri, RestTemplate restTemplate) throws URISyntaxException {
		TodoDto dto = TodoDto.getBuilder().description("Descripción todo").title("tarea").build();

		restTemplate.postForObject(rootUri, dto, TodoDto.class);
	}

	public static RestTemplate restTemplateFactory() {
		final ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new Jackson2HalModule());

		final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
		converter.setObjectMapper(mapper);

		return new RestTemplate(Collections.<HttpMessageConverter<?>> singletonList(converter));
	}

}