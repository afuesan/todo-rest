package es.leidra.rest.web.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.core.io.ClassPathResource;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerMapping;
import org.webjars.WebJarAssetLocator;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.leidra.rest.client.dtos.TodoDto;
import es.leidra.rest.client.dtos.TodoList;

@Controller
public class TodoController {
	private URI rootUri;
	private static final String FIND_BY_TITLE_URI = "/search/findAllByTitle?title={title}";
	private static final String FIND_ALL = "/search/findAll";

	@PostConstruct
	public void postConstruct() throws URISyntaxException {
		rootUri = new URI("http://dataaccess:8080/api/todos");
	}

	@RequestMapping(value = "/todo/item/{item}", method = RequestMethod.GET)
	public String item(@PathVariable("item") String item, Model model) {
		RestTemplate restTemplate = createRestTemplate();
		TodoDto dto = restTemplate.getForObject(rootUri + "/{id}", TodoDto.class, Long.valueOf(item));

		model.addAttribute("item", dto);
		return "todo/item";
	}

	@RequestMapping(value = { "/todo/list/{title}" }, method = RequestMethod.GET)
	public String search(@PathVariable(value = "title") String title, Model model) {
		RestTemplate restTemplate = createRestTemplate();
		TodoList dto = null;
		if (StringUtils.hasText(title)) {
			dto = restTemplate.getForObject(rootUri + FIND_BY_TITLE_URI, TodoList.class, title);
		} else {
			dto = restTemplate.getForObject(rootUri + FIND_ALL, TodoList.class);
		}
		model.addAttribute("todos", dto);
		return "todo/list";
	}

	private RestTemplate createRestTemplate() {
		final ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new Jackson2HalModule());

		final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
		converter.setObjectMapper(mapper);

		return new RestTemplate(Collections.<HttpMessageConverter<?>> singletonList(converter));
	}
	
	@ResponseBody
	@RequestMapping("/webjars/{webjar}/**")
	public ResponseEntity<?> locateWebjarAsset(@PathVariable String webjar, HttpServletRequest request) {
	    try {
	    	String mvcPrefix = "/webjars/" + webjar + "/"; // This prefix must match the mapping path!
	    	String mvcPath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);	    	
	        String fullPath = new WebJarAssetLocator().getFullPath(webjar, mvcPath.substring(mvcPrefix.length()));
	        return new ResponseEntity<>(new ClassPathResource(fullPath), HttpStatus.OK);
	    } catch (Exception e) {
	        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	}
}