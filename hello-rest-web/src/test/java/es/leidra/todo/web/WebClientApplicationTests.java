package es.leidra.todo.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.leidra.rest.web.WebClientApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WebClientApplication.class)
@WebAppConfiguration
public class WebClientApplicationTests {

	@Test
	public void contextLoads() {
	}

}
