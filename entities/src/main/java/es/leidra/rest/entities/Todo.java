package es.leidra.rest.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.util.Assert;

@Entity
public class Todo {
	static final int MAX_LENGTH_DESCRIPTION = 500;
	static final int MAX_LENGTH_TITLE = 100;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String description;
	private String title;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Todo() {
	}

	private Todo(Builder builder) {
		this.id = builder.id;
		this.description = builder.description;
		this.title = builder.title;
	}

	static Builder getBuilder() {
		return new Builder();
	}

	public void update(String title, String description) {
		checkTitleAndDescription(title, description);

		this.title = title;
		this.description = description;
	}


	public static class Builder {
		private String description;
		private String title;
		private Long id;

		private Builder() {
		}

		public Builder description(String description) {
			this.description = description;
			return this;
		}

		public Builder title(String title) {
			this.title = title;
			return this;
		}

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Todo build() {
			Todo build = new Todo(this);

			build.checkTitleAndDescription(build.getTitle(), build.getDescription());

			return build;
		}
	}

	private void checkTitleAndDescription(String title, String description) {
		Assert.notNull(title, "Title cannot be null");
		Assert.hasText(title, "Title cannot be empty");
		Assert.isTrue(title.length() <= MAX_LENGTH_TITLE, String.format("Title cannot be longer than %d characters", MAX_LENGTH_TITLE));

		if (description != null) {
			Assert.isTrue(description.length() <= MAX_LENGTH_DESCRIPTION, String.format("Description cannot be longer than %d characters", MAX_LENGTH_DESCRIPTION));
		}
	}
}