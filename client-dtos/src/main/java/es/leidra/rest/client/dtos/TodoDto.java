package es.leidra.rest.client.dtos;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TodoDto {
	static final int MAX_LENGTH_DESCRIPTION = 500;
	static final int MAX_LENGTH_TITLE = 100;

	private Long id;
	private String description;
	private String title;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TodoDto() {
	}

	private TodoDto(Builder builder) {
		this.description = builder.description;
		this.title = builder.title;
		this.id = builder.id;
	}

	public static Builder getBuilder() {
		return new Builder();
	}

	private void checkTitleAndDescription(String title, String description) {
		Assert.notNull(title, "Title cannot be null");
		Assert.hasText(title, "Title cannot be empty");
		Assert.isTrue(title.length() <= MAX_LENGTH_TITLE, String.format("Title cannot be longer than %d characters", MAX_LENGTH_TITLE));

		if (description != null) {
			Assert.isTrue(description.length() <= MAX_LENGTH_DESCRIPTION, String.format("Description cannot be longer than %d characters", MAX_LENGTH_DESCRIPTION));
		}
	}

	@Override
	public String toString() {
		return "TodoDto [id=" + id + ", description=" + description + ", title=" + title + "]";
	}

	public static class Builder {
		private String description;
		private String title;
		private Long id;

		private Builder() {
		}

		public Builder description(String description) {
			this.description = description;
			return this;
		}

		public Builder title(String title) {
			this.title = title;
			return this;
		}

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public TodoDto build() {
			TodoDto build = new TodoDto(this);

			build.checkTitleAndDescription(build.getTitle(), build.getDescription());

			return build;
		}
	}

}